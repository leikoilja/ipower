from iPower_dir import db
from iPower_dir.models import Category, Item


# create the database and the database table
db.create_all()

# insert category data
category1 = Category()
category1.category_title = "Construction"
category1.category_description = "DDD"


item1 = Item()
item1.category_id = 1
item1.name = 'Iron & Steel & Wire'
item1.distributor_url = "http://www.ayescelik.com/"
item1.href = "iron_steel_wire"
item1.picture_uri="steel"

db.session.add(category1)
db.session.add(item1)
# commit the changes
db.session.commit()