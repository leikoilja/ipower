from iPower_dir import db

class Category(db.Model):

    __tablename__ = "categories"

    id = db.Column(db.Integer, primary_key=True)
    category_title = db.Column(db.String, nullable=False)
    category_description = db.Column(db.String, nullable=False)
    items = db.relationship('Item', backref='categories', lazy=True)

class Item(db.Model):
    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(200),nullable=True)
    href = db.Column(db.String(20),nullable=True)
    distributor_url = db.Column(db.String(100), nullable=True)
    picture_uri = db.Column(db.String(50),nullable=True,default="none.png")

    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'), nullable=False)
