from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, SubmitField

class ContactForm(FlaskForm):
  name = TextField("Name")
  last_name = TextField("Last Name")
  email = TextField("Email")
  subject = TextField("Subject")
  message = TextAreaField("Message")
  submit = SubmitField("Send")
