# project/offer_sections/views.py

#################
#### imports ####
#################

from flask import render_template, Blueprint, request, flash
from iPower_dir.models import Category
from iPower_dir.offer_sections.forms import ContactForm
from flask_mail import Message
from iPower_dir import app, mail

################
#### config ####
################

offer_sections_blueprint = Blueprint('offer_sections', __name__, template_folder='templates')


################
#### routes ####
################

@offer_sections_blueprint.route('/')
def index():
    all_categories = Category.query.all()
    return render_template('index.html', categories=all_categories)

@offer_sections_blueprint.route('/brands')
def brands():
    return render_template('brands.html')

@offer_sections_blueprint.route('/about')
def about():
    return render_template('about.html')

@offer_sections_blueprint.route('/construction')
def construction():
    all_categories = Category.query.filter_by(category_title="Construction")
    return render_template('products/products_construction.html', categories=all_categories)

@offer_sections_blueprint.route('/hardware')
def hardware():
    all_categories = Category.query.filter_by(category_title="Hardware")
    return render_template('products/products_hardware.html', categories=all_categories)

@offer_sections_blueprint.route('/agriculture')
def agriculture():
    all_categories = Category.query.filter_by(category_title="Agriculture")
    return render_template('products/products_agriculture.html', categories=all_categories)

@offer_sections_blueprint.route('/other')
def other():
    all_categories = Category.query.filter_by(category_title="Project & Other")
    return render_template('products/products_other.html', categories=all_categories)

@offer_sections_blueprint.route('/contacts', methods=['GET', 'POST'])
def contact():
    form = ContactForm()

    if request.method == 'POST':
            msg = Message(form.subject.data, sender='info@ipowerindustry.com', recipients=['info@ipowerindustry.com'])
            msg.body = """
            This message is send from ipowerindustry.com home page.
            From: %s %s <%s>
            Message: %s
            """ % (form.name.data, form.last_name.data, form.email.data, form.message.data)
            mail.send(msg)
            app.logger.warning('Email send')
            return render_template('contact.html', success=True)

    elif request.method == 'GET':
        app.logger.warning('Showing contact page')
        return render_template('contact.html', form=form)