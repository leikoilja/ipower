#################
#### imports ####
#################

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_mail import Mail


################
#### config ####
################

app = Flask(__name__, instance_relative_config=True)
mail=Mail(app)
app.config.from_pyfile('flask.cfg')
mail = Mail(app)

db = SQLAlchemy(app)

# blueprints
from iPower_dir.users.views import users_blueprint
from iPower_dir.offer_sections.views import offer_sections_blueprint

# register the blueprints
app.register_blueprint(users_blueprint)
app.register_blueprint(offer_sections_blueprint)

#admin
#from iPower_dir.models import Category, Item
#admin = Admin(app, name='I-Power administation page', template_mode='bootstrap3')
# Add administrative views here
#admin.add_view(ModelView(Category, db.session))
#admin.add_view(ModelView(Item, db.session))


