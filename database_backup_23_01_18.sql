--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: ilja
--

CREATE TABLE categories (
    id integer NOT NULL,
    category_title character varying NOT NULL,
    category_description character varying NOT NULL
);


ALTER TABLE categories OWNER TO ilja;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: ilja
--

CREATE SEQUENCE categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO ilja;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ilja
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: ilja
--

CREATE TABLE items (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    description character varying(200),
    href character varying(20),
    distributor_url character varying(100),
    picture_uri character varying(50),
    category_id integer NOT NULL
);


ALTER TABLE items OWNER TO ilja;

--
-- Name: items_id_seq; Type: SEQUENCE; Schema: public; Owner: ilja
--

CREATE SEQUENCE items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE items_id_seq OWNER TO ilja;

--
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ilja
--

ALTER SEQUENCE items_id_seq OWNED BY items.id;


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: ilja
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: items id; Type: DEFAULT; Schema: public; Owner: ilja
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('items_id_seq'::regclass);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: ilja
--

COPY categories (id, category_title, category_description) FROM stdin;
1	Construction	We provide high quality construction metals, doors, ceramics and much more! 
2	Hardware	European quality tools, welding machines, generators, electrical equipment and light construction machines.
3	Agriculture	To make your agricultural experience better!
4	Project & Other	Everything you need for your project! 
\.


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: ilja
--

COPY items (id, name, description, href, distributor_url, picture_uri, category_id) FROM stdin;
1	Iron, Steel and Wire	Steel Bar, Iron Mesh, Black Wire, Barbled Wire, Razor Wire, Iron Fence	iron_steel_wire	http://www.ayescelik.com/galeri.html?lang=2	steel.jpg	1
2	PVC, Aluminium Door and Window, Glass	Interior Doors. Exterior Doors. Aluminium Doors. PVC Doors and PVC Windows	pvc	http://www.adopen.com/pvc-windows-and-door-systems/	door.png	1
3	Ceramic Adhesives	Gypsum Plaster, Gypsum Board, Tile Adhesive, Interior solutions and Exterior Solutions	gypsum	http://www.terraco.com.tr/	gypsum.jpg	1
4	Ceramic Tiles	Ceramic Tiles	ceramic_tiles	\N	ceramic_tiles.jpg	1
5	Sanitary Materials	Bathroom Set, Closet, Washbasin, Armature, Urinal, Seperator, Squatting Pan	sanitary	http://www.vitrine.com.tr/en	sanitary.jpg	1
6	Interior Decoration	Decoration, Lighting	interior	http://estetika.com.tr/	interior.jpg	1
7	Facade Cladding and Wall Systems	Panel Facade, Covered Facade, Silicon Facade, Transparent Facade, Semisilicon Facade, Locked in Glass Facade, Tailor Made Designs	facade	http://www.mayem.com.tr	facade.jpg	1
8	Paints & Brushes	Interior, Exterior, Primes, Decorative, Isolation	paints	http://www.birlesikfirca.com/	paints.jpg	1
9	Pipes and Fittings	Infrastructure Pipes, Building Pipes, Agricultural Pipes, Polyethilen Systems	pipes	http://www.egeplast.com.tr	pipes.jpg	1
10	Solar Power Products	Solar Panels, Solar Heaters, Depots, Collectors	solar	http://www.eraslan.com.tr	solar.jpg	1
11	Hand and Power Tools	Tools as Hammer, Plier, Screwdriver, Clipper, Circuit, Tester, Drills, Wall Grinding, Cutting Machine, Grinders, Saws, Sanders, Mixer, Battery Charger, Air Compressor, High Pressure Washer and Pumps	tools	http://www.aktekltd.com	tools.jpg	2
12	Cutting & Grinding Discs	Cutting Disc, Grinding Disc and Inox Disc	disks	http://www.karbosan.com.tr	disks.jpg	2
13	Insulation Mastics	Insulation Mastics, Mircellaneous Chemicals, Injection Guns	mastics	http://www.akfix.com	mastics.jpg	2
14	Welding Machines & Accessories	MMA Welding, Mig Mag Welding, Tig Welding, Subarc Welding, Stick Electrodes, Tungsten Electrodes, Carbon Electrodes, Welding Wires	welding	http://www.kilmak.com	welding.jpg	2
15	Light Construction Machineries	Cylinders, Compactor, Vibrators, Asphalt cutters, Concrete Machines, Concrete Shapers, Cranes and Lighting Towers	light_machines	http://www.anadolumotor.com.tr	light_machines.png	2
16	Generator Sets & Forklifts	10 -2000 kVA generators, Forklifts, Portable generators, Welding generators	generators	http://ilken.com.tr/	generator_forklift.jpg	2
17	Portative Generators & Welding Generator	After Sales Services, Spare Parts, Installation	portative_generators	http://www.anadolumotor.com.tr	portable_generators.jpg	2
18	Electricity Materials	Sockets, Frames, Fuses, Plugs, Lights, Contactors, Led lights, Plugs, Counters, Switches, Tapes, Dimmers, Cable channels, Sockets, Transformators, Communication Products, Others	electricity	http://www.ne-ad.com.tr/	electricity.jpg	2
19	Cables	Low Voltage, Halogen Free, Medium Voltage Cables, High Voltage Cables	cables	http://www.ne-ad.com.tr/	cables.jpg	2
20	Tractor	Garden Type Tractors, 2WD Tractors, 4WD Tractors, Power Tillers, Tiller Attachments	tractors	http://www.anadolumotor.com.tr	tractors.jpg	3
21	Cultivators & Transportation Vehicles	After Sales Services and Spare Part	cultivators	http://www.anadolumotor.com.tr	cultivators.jpg	3
22	Motopump	Diesel Motopumps and Gasoline Motopumps	motopump	http://www.anadolumotor.com.tr	motopump.jpg	3
23	Chainsaw	Different types of chainsaws	chainsaw	http://www.anadolumotor.com.tr	chainsaw.jpg	3
24	Garden equipments	Brushcutters, Hedge Trimmers, Grasscutters, Lawnmowers	garden	http://www.anadolumotor.com.tr	garden.jpg	3
25	Irrigation equipment & Pipes	Motopumps, Irrigation Pipes	irrigation	http://www.anadolumotor.com.tr	irrigation.jpg	3
26	Industrial kitchen equipment	Pizza Ovens, Toasters, Fryers, Grills & Creperies, Spatula	kitchen	http://www.eurogastrostar.com	kitchen.jpg	4
27	Silo for storage & transportation	Silo, Storage Depots, Loading/Offloading Units, Transport Systems	silo	http://5smak.com.tr	silo.jpg	4
28	Game parks	Wooden Playgrounds, Playgrounds for Disables, Eco Metal Playgrounds, High Tower Playgrounds, Polyethylene Playgrounds, Picnic Tables, Gazebos, Benches, Lightning, Dustbins, Outdoor Sport Equipment	game_park	http://dusepark.com/	game_parks.jpg	4
29	Plastic House Materials	Plastic Materials, Hangers, Brushes, Bathroom Accessories, Dispensers	plastic	http://cetinplastik.com/	plastic_tools.jpg	4
30	Safety Clothings & Equipments	Safety Footwear, Electrician Shoes, Military Boats, Fire Fighter & Rescue, Motorcycle Boats, Officer Foot Wear, Uniforms, Military and Personal Safety Equipment	safety	http://fostershoes.com/	safety.jpg	4
31	Hospital Equipments	Patient Beds, Overbeds Tables, Companion Chairs, Wheel Chairs, Examination Tables, Gyaecological Examination Tables, Medicine Treatment, Other Hospital Equipment, Stainless Equipment, Baby Incubators	hospital	http://samatip.com/	hospital.jpg	4
32	Cooling Systems	Group Cooling, Cold Storage Panels, Cold Room Doors, Cold Room Accessories, Market Cases, Ice Machines	cooling	http://bessogutma.com/	cooling.jpg	4
33	Chemicals	Natural Stone Chemicals, Construction Chemicals, Automotive Chemicals	chemicals	http://www.elkay.com.tr	chemicals.jpg	4
34	PVC & Aluminium Processing Machines	Saws, Welding & Cleaning, Routers, Roller Tables, Pvc Line, Punch Press, Settlement Plans, CNC Machines	pvc_machines	http://yilmazmachine.com.tr	pvc_machines.jpg	4
35	Construction Solutions	We build offices, housing, factories, bank branches, cinema halls, stores, restaurants and much more	construction_sol	http://www.akfaholding.com/	construction_solutions.jpg	4
36	Health & Hospital Solutions	A wide selection of medical furniture, beds and hospital installation equipment	medical	http://www.akfaholding.com/	medical.jpg	4
37	Electromechanic Solutions	IT SOLUTIONS, SECURITY, AUTOMATION,  INTEGRATION, BUILDING ELECTRONICS, PROFESSIONAL A/V/L SOLUTIONS	electromechanic	http://www.akfaholding.com/	electromechanical.jpg	4
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ilja
--

SELECT pg_catalog.setval('categories_id_seq', 4, true);


--
-- Name: items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ilja
--

SELECT pg_catalog.setval('items_id_seq', 37, true);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: ilja
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: items items_pkey; Type: CONSTRAINT; Schema: public; Owner: ilja
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- Name: items items_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ilja
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- PostgreSQL database dump complete
--

