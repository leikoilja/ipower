# run.py

from iPower_dir import app
import logging
from logging.handlers import RotatingFileHandler

if __name__ == "__main__":
    handler = RotatingFileHandler('iPower.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.ERROR)
    app.logger.addHandler(handler)

    app.run(host='0.0.0.0')